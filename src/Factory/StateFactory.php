<?php

namespace App\Factory;

use App\Entity\State;
use App\Repository\StateRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static State|Proxy createOne(array $attributes = [])
 * @method static State[]|Proxy[] createMany(int $number, $attributes = [])
 * @method static State|Proxy findOrCreate(array $attributes)
 * @method static State|Proxy random(array $attributes = [])
 * @method static State|Proxy randomOrCreate(array $attributes = [])
 * @method static State[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static State[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static StateRepository|RepositoryProxy repository()
 * @method State|Proxy create($attributes = [])
 */
final class StateFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://github.com/zenstruck/foundry#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://github.com/zenstruck/foundry#model-factories)
        ];
    }

    protected function initialize(): self
    {
        // see https://github.com/zenstruck/foundry#initialization
        return $this
            // ->afterInstantiate(function(State $state) {})
        ;
    }

    protected static function getClass(): string
    {
        return State::class;
    }
}
