<?php

namespace App\Factory;

use App\Entity\CompetitorProduct;
use App\Repository\CompetitorProductRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static CompetitorProduct|Proxy createOne(array $attributes = [])
 * @method static CompetitorProduct[]|Proxy[] createMany(int $number, $attributes = [])
 * @method static CompetitorProduct|Proxy findOrCreate(array $attributes)
 * @method static CompetitorProduct|Proxy random(array $attributes = [])
 * @method static CompetitorProduct|Proxy randomOrCreate(array $attributes = [])
 * @method static CompetitorProduct[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static CompetitorProduct[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static CompetitorProductRepository|RepositoryProxy repository()
 * @method CompetitorProduct|Proxy create($attributes = [])
 */
final class CompetitorProductFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://github.com/zenstruck/foundry#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            'game' => GameFactory::random(),
            'state' => StateFactory::random(),
            'seller' => SellerFactory::random(),
            'price' => self::faker()->randomFloat(2, 0, 100),
        ];
    }

    protected function initialize(): self
    {
        // see https://github.com/zenstruck/foundry#initialization
        return $this
            // ->afterInstantiate(function(CompetitorProduct $competitorProduct) {})
        ;
    }

    protected static function getClass(): string
    {
        return CompetitorProduct::class;
    }
}
