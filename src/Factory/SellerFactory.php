<?php

namespace App\Factory;

use App\Entity\Seller;
use App\Repository\SellerRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static Seller|Proxy createOne(array $attributes = [])
 * @method static Seller[]|Proxy[] createMany(int $number, $attributes = [])
 * @method static Seller|Proxy findOrCreate(array $attributes)
 * @method static Seller|Proxy random(array $attributes = [])
 * @method static Seller|Proxy randomOrCreate(array $attributes = [])
 * @method static Seller[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Seller[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static SellerRepository|RepositoryProxy repository()
 * @method Seller|Proxy create($attributes = [])
 */
final class SellerFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://github.com/zenstruck/foundry#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            'name' => self::faker()->company
        ];
    }

    protected function initialize(): self
    {
        // see https://github.com/zenstruck/foundry#initialization
        return $this
            // ->afterInstantiate(function(Seller $seller) {})
        ;
    }

    protected static function getClass(): string
    {
        return Seller::class;
    }
}
