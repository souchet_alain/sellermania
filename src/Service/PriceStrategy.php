<?php


namespace App\Service;


use App\Entity\CompetitorProduct;
use App\Entity\Game;
use App\Entity\Product;
use App\Entity\State;
use Exception;

class PriceStrategy
{
    const SAME_STATE_DISCOUNT = 0.01;
    const WORSE_STATE_DISCOUNT = 1;

    /**
     * Return competitors products corresponding to the given game
     * @param Game $game
     * @param array $competitorsProducts
     * @return array
     */
    public function getCompetitorsProductsByGame(Game $game, array $competitorsProducts): array
    {
        $selectedProducts = [];

        /** @var CompetitorProduct $product */
        foreach ($competitorsProducts as $product) {
            if ($product->getGame() === $game) {
                $selectedProducts[] = $product;
            }
        }

        return $selectedProducts;
    }

    /**
     * Return competitors products corresponding to the given state
     * @param State $state
     * @param array $competitorsProducts
     * @return array
     */
    public function getCompetitorsProductsByState(State $state, array $competitorsProducts): array
    {
        $selectedProducts = [];

        /** @var CompetitorProduct $competitorProduct */
        foreach ($competitorsProducts as $competitorProduct) {
            if ($competitorProduct->getState() === $state) {
                $selectedProducts[] = $competitorProduct;
            }
        }

        return $selectedProducts;
    }

    /**
     * Return competitors products with state higher than the given state
     * @param State $state
     * @param array $competitorsProducts
     * @return array
     */
    public function getCompetitorsProductsWithHigherState(State $state, array $competitorsProducts): array
    {
        $selectedProducts = [];

        /** @var CompetitorProduct $competitorProduct */
        foreach ($competitorsProducts as $competitorProduct) {
            if ($competitorProduct->getState()->getLevel() > $state->getLevel()) {
                $selectedProducts[] = $competitorProduct;
            }
        }

        return $selectedProducts;
    }

    /**
     * Compute product price taking in account a list of competitors products
     * @param Product $product
     * @param array $competitorsProducts
     * @return array|float[]|int[]
     * @throws Exception
     */
    public function computeProductPrice(Product $product, array $competitorsProducts): array
    {
        // Get competitors products with same game than product
        $competitorsProductsSameGame = $this->getCompetitorsProductsByGame($product->getGame(), $competitorsProducts);

        if (count($competitorsProductsSameGame) > 0) {
            return $this->computePriceSameGame($product, $competitorsProductsSameGame);
        } else {
            // If there is no competitors products with the same game, the price can be anything greater than floor price
            return ['gt' => $product->getFloorPrice()];
        }
    }

    /**
     * Competitors products comparison taking in account the price
     * @param CompetitorProduct $product1
     * @param CompetitorProduct $product2
     * @return int
     */
    private static function compareCompetitorsProductsByPrice(CompetitorProduct $product1, CompetitorProduct $product2): int
    {
        // sort by ascending price
        return $product1->getPrice() <=> $product2->getPrice();
    }

    /**
     * Compute product price taking in account competitor products with the same game
     * @param Product $product
     * @param array $competitorsProductsSameGame
     * @return float[]|int[]
     * @throws Exception
     */
    private function computePriceSameGame(Product $product, array $competitorsProductsSameGame): array
    {
        // Get competitors products with the same state (and game) than the product
        $competitorsProductsSameState = $this->getCompetitorsProductsByState($product->getState(), $competitorsProductsSameGame);

        if (count($competitorsProductsSameState) > 0) {
            return $this->computePriceSameGameState($product, $competitorsProductsSameState);
        } else {
            return $this->computePriceSameGameDifferentState($product, $competitorsProductsSameGame);
        }
    }

    /**
     * Compute product price taking in account competitors products with same game and state
     * @param Product $product
     * @param array $competitorsProductsSameState
     * @return float[]
     * @throws Exception
     */
    private function computePriceSameGameState(Product $product, array $competitorsProductsSameState): array
    {
        if (count($competitorsProductsSameState) === 0) {
            throw new Exception("The competitors products array can't be empty");
        }

        // Sort competitors products by ascending prices
        usort($competitorsProductsSameState, [self::class, "compareCompetitorsProductsByPrice"]);

        // Competitor product with the lowest price
        /** @var $competitorProductWithLowestPrice CompetitorProduct */
        $competitorProductWithLowestPrice = $competitorsProductsSameState[0];

        $price = $competitorProductWithLowestPrice->getPrice() - self::SAME_STATE_DISCOUNT;

        return ['eq' => ($price > $product->getFloorPrice()) ? $price : $product->getFloorPrice()];
    }

    /**
     * Compute product price taking in account competitor products with same game but different states
     * @param Product $product
     * @param array $competitorsProductsSameGame
     * @return int[]
     * @throws Exception
     */
    private function computePriceSameGameDifferentState(Product $product, array $competitorsProductsSameGame): array
    {
        if (count($competitorsProductsSameGame) === 0) {
            throw new Exception("The competitors products array can't be empty");
        }

        // Get competitors products whith state higher than product's state
        $selectedCompetitorsProducts = $this->getCompetitorsProductsWithHigherState($product->getState(), $competitorsProductsSameGame);

        if (count($selectedCompetitorsProducts) > 0) {
            // If there are products with higher state, get the one with the lowest price and substract the corresponding discount
            usort($selectedCompetitorsProducts, [self::class, "compareCompetitorsProductsByPrice"]);

            /** @var CompetitorProduct $selectedCompetitorsProductsWithLowestPrice */
            $selectedCompetitorsProductsWithLowestPrice = $selectedCompetitorsProducts[0];

            $comp = 'eq';
            $price = $selectedCompetitorsProductsWithLowestPrice->getPrice() - self::WORSE_STATE_DISCOUNT;
        } else {
            // If all competitors products have a lower state, the price must be higher than the most expensive
            usort($competitorsProductsSameGame, [self::class, "compareCompetitorsProductsByPrice"]);

            /** @var CompetitorProduct $selectedCompetitorsProductsWithHighestPrice */
            $selectedCompetitorsProductsWithHighestPrice = $competitorsProductsSameGame[count($competitorsProductsSameGame) - 1];

            $comp = 'gt';
            $price = $selectedCompetitorsProductsWithHighestPrice->getPrice();
        }

        return [$comp => ($price > $product->getFloorPrice()) ? $price : $product->getFloorPrice()];
    }
}
