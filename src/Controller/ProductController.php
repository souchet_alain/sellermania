<?php

namespace App\Controller;

use App\Entity\Game;
use App\Entity\State;
use App\Repository\CompetitorProductRepository;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/products-list", name="products_list")
     * @param ProductRepository $productRepository
     * @return Response
     */
    public function viewProductsList(ProductRepository $productRepository): Response
    {
        $products = $productRepository->getProductsOrderedByStateName();
        return $this->render('product/products-list.html.twig', [
            'products' => $products,
        ]);
    }
}
