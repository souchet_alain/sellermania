<?php

namespace App\Controller;

use App\Entity\Game;
use App\Entity\Product;
use App\Form\ProductFormType;
use App\Repository\CompetitorProductRepository;
use App\Repository\GameRepository;
use App\Service\PriceStrategy;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GameController extends AbstractController
{
    /**
     * @Route("/game/{game}", name="game")
     * @param Game $game
     * @param GameRepository $gameRepository
     * @param CompetitorProductRepository $competitorProductRepository
     * @param PriceStrategy $priceStrategy
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function viewGame(Game $game,
                             GameRepository $gameRepository,
                             CompetitorProductRepository $competitorProductRepository,
                             PriceStrategy $priceStrategy,
                             Request $request): Response
    {
        $game = $gameRepository->find($game);
        $competitorsProducts = $competitorProductRepository->getCompetitorsProductsByGame($game);
        $price = null;

        $form = $this->createForm(ProductFormType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Product $product */
            $product = $form->getData();
            $product->setGame($game);

            $price = $priceStrategy->computeProductPrice($product, $competitorsProducts);
        }

        return $this->render('game/game.html.twig', [
            'game' => $game,
            'productForm' => $form->createView(),
            'floorPrice' => isset($product) ? $product->getFloorPrice() : 0,
            'price' => $price,
            'competitorsProducts' => $competitorsProducts,
        ]);
    }
}
