<?php

namespace App\Form;

use App\Entity\Product;
use App\Entity\State;
use App\Repository\StateRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductFormType extends AbstractType
{
    /**
     * @var StateRepository
     */
    private $stateRepository;

    public function __construct(StateRepository $stateRepository)
    {
        $this->stateRepository = $stateRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('state', EntityType::class, [
                'class' => State::class,
                'choice_label' => 'label',
                'choices' => $this->stateRepository->getStatesOrderByLevel(),
                'label' => 'État',
                'placeholder' => 'Choisir un état',
                'invalid_message' => 'État invalide',
            ])
            ->add('floorPrice', NumberType::class, [
                'label' => 'Prix plancher',
                'scale' => 2,
                'invalid_message' => 'Veuillez saisir un prix plancher',
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
