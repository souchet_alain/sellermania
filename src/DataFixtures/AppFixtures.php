<?php

namespace App\DataFixtures;

use App\Entity\State;
use App\Factory\CompetitorProductFactory;
use App\Factory\GameFactory;
use App\Factory\ProductFactory;
use App\Factory\SellerFactory;
use App\Factory\StateFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    const AVAILABLE_STATES = [
        0 => 'moyen',
        1 => 'bon',
        2 => 'très bon',
        3 => 'comme neuf',
        4 => 'neuf',
    ];

    public function load(ObjectManager $manager)
    {
        // Games
        GameFactory::new()->createMany(12);

        // States
        foreach (self::AVAILABLE_STATES as $level => $stateLabel) {
            StateFactory::createOne(['level' => $level, 'label' => $stateLabel]);
        }

        // Sellers
        SellerFactory::new()->createMany(8);

        // Products
        ProductFactory::new()->createMany(20);

        // Competitors products
        CompetitorProductFactory::new()->createMany(50);
    }
}
