<?php

namespace App\Repository;

use App\Entity\CompetitorProduct;
use App\Entity\Game;
use App\Entity\State;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CompetitorProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompetitorProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompetitorProduct[]    findAll()
 * @method CompetitorProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompetitorProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompetitorProduct::class);
    }

    /**
     * Get all products from competitors relative to a given game and state
     *
     * @param Game $game
     * @param State $state
     * @return int|mixed|string
     */
    public function getCompetitorsProductsByGameState(Game $game, State $state)
    {
        return $this->createQueryBuilder('product')
            ->andWhere('product.game = :game')
            ->andWhere('product.state = :state')
            ->setParameter('game', $game)
            ->setParameter('state', $state)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * Get all products from competitors relative to a given game ordered by state and price
     *
     * @param Game $game
     * @return int|mixed|string
     */
    public function getCompetitorsProductsByGame(Game $game)
    {
        return $this->createQueryBuilder('product')
            ->andWhere('product.game = :game')
            ->setParameter('game', $game)
            ->leftJoin('product.state', 'product_state')
            ->orderBy('product_state.level', 'DESC')
            ->addOrderBy('product.price', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }
}
