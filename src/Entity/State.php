<?php

namespace App\Entity;

use App\Repository\StateRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StateRepository::class)
 */
class State
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="state")
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity=CompetitorProduct::class, mappedBy="state")
     */
    private $competitorProducts;

    /**
     * @ORM\Column(type="integer", unique=true)
     */
    private $level;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->competitorProducts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setState($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getState() === $this) {
                $product->setState(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CompetitorProduct[]
     */
    public function getCompetitorProducts(): Collection
    {
        return $this->competitorProducts;
    }

    public function addCompetitorProduct(CompetitorProduct $competitorProduct): self
    {
        if (!$this->competitorProducts->contains($competitorProduct)) {
            $this->competitorProducts[] = $competitorProduct;
            $competitorProduct->setState($this);
        }

        return $this;
    }

    public function removeCompetitorProduct(CompetitorProduct $competitorProduct): self
    {
        if ($this->competitorProducts->removeElement($competitorProduct)) {
            // set the owning side to null (unless already changed)
            if ($competitorProduct->getState() === $this) {
                $competitorProduct->setState(null);
            }
        }

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function __toString()
    {
        return $this->label;
    }


}
