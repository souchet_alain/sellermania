<?php

namespace App\Entity;

use App\Repository\GameRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GameRepository::class)
 */
class Game
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $developedBy;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $publishedBy;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $year;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="game")
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity=CompetitorProduct::class, mappedBy="game")
     */
    private $competitorProducts;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->competitorProducts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDevelopedBy(): ?string
    {
        return $this->developedBy;
    }

    public function setDevelopedBy(?string $developedBy): self
    {
        $this->developedBy = $developedBy;

        return $this;
    }

    public function getPublishedBy(): ?string
    {
        return $this->publishedBy;
    }

    public function setPublishedBy(string $publishedBy): self
    {
        $this->publishedBy = $publishedBy;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(?int $year): self
    {
        $this->year = $year;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setGame($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getGame() === $this) {
                $product->setGame(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CompetitorProduct[]
     */
    public function getCompetitorProducts(): Collection
    {
        return $this->competitorProducts;
    }

    public function addCompetitorProduct(CompetitorProduct $competitorProduct): self
    {
        if (!$this->competitorProducts->contains($competitorProduct)) {
            $this->competitorProducts[] = $competitorProduct;
            $competitorProduct->setGame($this);
        }

        return $this;
    }

    public function removeCompetitorProduct(CompetitorProduct $competitorProduct): self
    {
        if ($this->competitorProducts->removeElement($competitorProduct)) {
            // set the owning side to null (unless already changed)
            if ($competitorProduct->getGame() === $this) {
                $competitorProduct->setGame(null);
            }
        }

        return $this;
    }
}
