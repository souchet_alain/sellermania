<?php

namespace App\Entity;

use App\Repository\SellerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SellerRepository::class)
 */
class Seller
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=CompetitorProduct::class, mappedBy="seller")
     */
    private $competitorProducts;

    public function __construct()
    {
        $this->competitorProducts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|CompetitorProduct[]
     */
    public function getCompetitorProducts(): Collection
    {
        return $this->competitorProducts;
    }

    public function addCompetitorProduct(CompetitorProduct $competitorProduct): self
    {
        if (!$this->competitorProducts->contains($competitorProduct)) {
            $this->competitorProducts[] = $competitorProduct;
            $competitorProduct->setSeller($this);
        }

        return $this;
    }

    public function removeCompetitorProduct(CompetitorProduct $competitorProduct): self
    {
        if ($this->competitorProducts->removeElement($competitorProduct)) {
            // set the owning side to null (unless already changed)
            if ($competitorProduct->getSeller() === $this) {
                $competitorProduct->setSeller(null);
            }
        }

        return $this;
    }
}
