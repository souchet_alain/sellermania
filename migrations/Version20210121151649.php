<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210121151649 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE competitor_product (id INT AUTO_INCREMENT NOT NULL, game_id INT NOT NULL, state_id INT NOT NULL, seller_id INT NOT NULL, price DOUBLE PRECISION NOT NULL, INDEX IDX_5CCCA57FE48FD905 (game_id), INDEX IDX_5CCCA57F5D83CC1 (state_id), INDEX IDX_5CCCA57F8DE820D9 (seller_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE game (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, developed_by VARCHAR(255) DEFAULT NULL, published_by VARCHAR(255) NOT NULL, year INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, game_id INT NOT NULL, state_id INT NOT NULL, floor_price DOUBLE PRECISION NOT NULL, INDEX IDX_D34A04ADE48FD905 (game_id), INDEX IDX_D34A04AD5D83CC1 (state_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE seller (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE state (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE competitor_product ADD CONSTRAINT FK_5CCCA57FE48FD905 FOREIGN KEY (game_id) REFERENCES game (id)');
        $this->addSql('ALTER TABLE competitor_product ADD CONSTRAINT FK_5CCCA57F5D83CC1 FOREIGN KEY (state_id) REFERENCES state (id)');
        $this->addSql('ALTER TABLE competitor_product ADD CONSTRAINT FK_5CCCA57F8DE820D9 FOREIGN KEY (seller_id) REFERENCES seller (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADE48FD905 FOREIGN KEY (game_id) REFERENCES game (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD5D83CC1 FOREIGN KEY (state_id) REFERENCES state (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE competitor_product DROP FOREIGN KEY FK_5CCCA57FE48FD905');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADE48FD905');
        $this->addSql('ALTER TABLE competitor_product DROP FOREIGN KEY FK_5CCCA57F8DE820D9');
        $this->addSql('ALTER TABLE competitor_product DROP FOREIGN KEY FK_5CCCA57F5D83CC1');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD5D83CC1');
        $this->addSql('DROP TABLE competitor_product');
        $this->addSql('DROP TABLE game');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE seller');
        $this->addSql('DROP TABLE state');
    }
}
