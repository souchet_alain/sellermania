// jQuery
import $ from 'jquery';

// bootstrap
import 'bootstrap';

// Custom css
import './styles/app.scss';

$(document).ready(function() {
    $('tr').on('click', function() {
        window.location.href = $(this).data('game-link');
    });
});
